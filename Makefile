NAME='registry.gitlab.com/tikal-external/academy-public/demo-nodejs-http-server'

build:
	docker build -t $(NAME) .

run:
	docker run -it --rm -p 8080:8080 $(NAME)

push:
	docker push $(NAME)
